#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <DNSServer.h>       //Local DNS Server used for redirecting all requests to the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic

const int TX_PIN = 5;   // NodeMCU pin D1 = GPIO 5

typedef struct  {
    boolean power;
    int duration;   // micro seconds
} signal;

const signal SHORT_ON { true, 400 };
const signal SHORT_OFF { false, 500 };
const signal LONG_ON { true, 850 };
const signal LONG_OFF { false, 950 };
const int REST = 10000;

const char* host = "fancontrol";

MDNSResponder mdns;
ESP8266WebServer server ( 80 );

signal sig_preamble [] {   // 50 items total
    SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF, SHORT_ON, LONG_OFF
};

signal sig_light [] {
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_power [] {
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_1 [] {
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_2 [] {
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_3 [] {
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_4 [] {
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_5 [] {
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_6 [] {
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_breeze [] {
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_house [] {
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_delay [] {
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_2h [] {
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_4h [] {
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_8h [] {
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_summer [] {
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

signal sig_winter [] {
    SHORT_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, SHORT_OFF,
    LONG_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, LONG_OFF,
    SHORT_ON, SHORT_OFF,
    LONG_ON, LONG_OFF
};

void setup ( void ) {
    pinMode(TX_PIN, OUTPUT);
    Serial.begin ( 115200 );
    Serial.println ( "Initializing" );
    WiFiManager wifiManager;
    wifiManager.autoConnect(host, "esp8266");

    if ( mdns.begin ( host, WiFi.localIP() ) ) {
        Serial.println ( "MDNS responder started" );
    }
    bool result = SPIFFS.begin();
    if (result != true) { SPIFFS.format();}
    Serial.println("SPIFFS opened: " + result);
    server.on("/control", HTTP_ANY, commandSet);    // don't need to handle GET as Fan Control is a one way transmission
    server.onNotFound ( handleNotFound );   // actually, "not specified" - will get resources from file system
    server.begin();
    Serial.println ( "HTTP server started" );
}

void doTransmission(signal array[16]) {
    Serial.println("Doing transmission...");
    for (int burst = 1; burst <= 6; burst++) {
        for (int pre = 0; pre < 32; ++pre) {    // Preamble
            digitalWrite(TX_PIN, sig_preamble[pre].power);
            delayMicroseconds (sig_preamble[pre].duration);
        }
        for (int idx = 0; idx < 16; ++idx ) {   // payload
            digitalWrite(TX_PIN, array[idx].power);
            delayMicroseconds( array[idx].duration );
        }
        digitalWrite(TX_PIN, SHORT_ON.power);   // postamble - is that a word?
        delayMicroseconds(SHORT_ON.duration);
        digitalWrite(TX_PIN, SHORT_OFF.power);
        delayMicroseconds(REST);                // rest between bursts
    }
}

void commandSet()
{
    String operation = server.arg("operation");
    Serial.print("Operation = ");
    Serial.println(operation);

    if (operation == "light") { doTransmission(sig_light); }
    else if (operation == "power") { doTransmission(sig_power); }
    else if (operation == "1") { doTransmission(sig_1); }
    else if (operation == "2") { doTransmission(sig_2); }
    else if (operation == "3") { doTransmission(sig_3); }
    else if (operation == "4") { doTransmission(sig_4); }
    else if (operation == "5") { doTransmission(sig_5); }
    else if (operation == "6") { doTransmission(sig_6); }
    else if (operation == "random_fan") { doTransmission(sig_breeze); }
    else if (operation == "random_light") { doTransmission(sig_house); }
    else if (operation == "delay") { doTransmission(sig_delay); }
    else if (operation == "2H") { doTransmission(sig_2h); }
    else if (operation == "4H") { doTransmission(sig_4h); }
    else if (operation == "8H") { doTransmission(sig_8h); }
    else if (operation == "summer") { doTransmission(sig_summer); }
    else if (operation == "winter") { doTransmission(sig_winter); }

    server.send ( 200, "text/plain", "OK" );
}

void handleNotFound() {
    if(!handleFileRead(server.uri())) {
        server.send(404, "text/plain", "FileNotFound");
    }
}

bool handleFileRead(String path){
    Serial.println("handleFileRead: " + path);
    if(path.endsWith("/")) path += "index.html";
    String contentType = getContentType(path);
    String pathWithGz = path + ".gz";
    if(SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)){
        if(SPIFFS.exists(pathWithGz))
            path += ".gz";
        File file = SPIFFS.open(path, "r");
        size_t sent = server.streamFile(file, contentType);
        file.close();
        return true;
    }
    return false;
}

String getContentType(String filename){
    if(server.hasArg("download")) return "application/octet-stream";
    else if(filename.endsWith(".htm")) return "text/html";
    else if(filename.endsWith(".html")) return "text/html";
    else if(filename.endsWith(".css")) return "text/css";
    else if(filename.endsWith(".js")) return "application/javascript";
    else if(filename.endsWith(".png")) return "image/png";
    else if(filename.endsWith(".gif")) return "image/gif";
    else if(filename.endsWith(".jpg")) return "image/jpeg";
    else if(filename.endsWith(".ico")) return "image/x-icon";
    else if(filename.endsWith(".xml")) return "text/xml";
    else if(filename.endsWith(".pdf")) return "application/x-pdf";
    else if(filename.endsWith(".zip")) return "application/x-zip";
    else if(filename.endsWith(".gz")) return "application/x-gzip";
    return "text/plain";
}

void loop ( void ) {
    mdns.update();
    server.handleClient();
}
